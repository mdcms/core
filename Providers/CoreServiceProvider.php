<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
		$this->registerViews();
		$this->registerValidator();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerConfig();
		$this->app['router']->aliasMiddleware('permissions', 'Modules\Core\Http\Middleware\Permissions');
	}

	/**
	 * Register config.
	 *
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
			__DIR__ . '/../Config/config.php' => config_path('core.php'),
		], 'config');
		$this->mergeConfigFrom(
			__DIR__ . '/../Config/config.php', 'core'
		);
	}

	/**
	 * Register views.
	 *
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/core');

		$sourcePath = __DIR__ . '/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path . '/modules/core';
		}, \Config::get('view.paths')), [$sourcePath]), 'core');
	}

	/**
	 * Register translations.
	 *
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/core');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'core');
		} else {
			$this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'core');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

	protected function registerValidator()
	{
		\Validator::extend('slug', function ($attribute, $value, $parameters, $validator) {
			if ($this->checkSlug($value)) return true;
			else return false;

		});
	}

	protected function checkSlug($slug)
	{
		if (preg_match('/^[a-z0-9][a-z0-9-_]+[a-z0-9]$/', $slug)) {
			foreach (['--', '__', '-_', '_-'] as $v)
				if (strpos($slug, $v))
					return false;
			return true;
		}
		return false;
	}
}
