<?php
namespace Modules\Core\Repositories;

interface BaseRepository
{
	public function find($id);
}