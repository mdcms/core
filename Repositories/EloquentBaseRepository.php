<?php
namespace Modules\Core\Repositories;

abstract class EloquentBaseRepository implements BaseRepository
{
	protected $model;

	public function __construct($model)
	{
		$this->model = $model;
	}

	public function find($id)
	{
		return $this->model->find($id);
	}
}